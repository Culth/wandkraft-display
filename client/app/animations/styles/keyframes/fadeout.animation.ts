import { style } from '@angular/animations';

export const fadeOut = [
	style(
		{
			opacity: '1'
		}
	),
	style(
		{
			opacity: '0'
		}
	)
];