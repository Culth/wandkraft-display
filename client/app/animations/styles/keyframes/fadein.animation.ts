import { style } from '@angular/animations';

export const fadeIn = [
	style(
		{
			opacity: '0'
		}
	),
	style(
		{
			opacity: '1'
		}
	)
];