import { style } from '@angular/animations';

export const fadeInUp = [
	style(
		{
			opacity: '0',
			transform: 'translateY(30px)'
		}
	),
	style(
		{
			opacity: '1',
			transform: 'translateY(0)'
		}
	)
];