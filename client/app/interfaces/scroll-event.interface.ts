export interface ScrollEventInterface {
	position: number;
	transition: boolean;
	transitionDuration: string;
	transitionEase: string;
}