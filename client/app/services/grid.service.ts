import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

/**
 * @export
 * @class GridService
 */
@Injectable({
  providedIn: 'root'
})
export class GridService 
{

  /**
   * @private
   * @memberof GridService
   */
  private subject = new Subject<number[]>();

  /**
   * @type {number[]}
   * @memberof GridService
   */
  public columns: number[] = [ 0, 0, 0, 0 ];
  
  /**
   * Clear columns
   *
   * @memberof GridService
   */
  public clear() 
  {
    this.columns = this.columns.map(i => 0);
  }

  /**
   * Get All Columns
   *
   * @returns
   * @memberof GridService
   */
  public getColumns() 
  {
    return this.columns;
  }

  /**
   * Get Column count
   *
   * @returns
   * @memberof GridService
   */
  public getColumnWidth() 
  {
    return 100 / this.getColumns().length;
  }

  /**
   * Get largest column
   *
   * @returns
   * @memberof GridService
   */
  public getMax() 
  {
    const value = Math.max.apply(Math, this.columns);
    const index = this.columns.indexOf(value);

    return {
      'index': index,
      'value': value
    };
  }

  /**
   * Get smallest column
   *
   * @returns
   * @memberof GridService
   */
  public getMin() 
  {
    const value = Math.min.apply(Math, this.columns);
    const index = this.columns.indexOf(value);

    return {
      'index': index,
      'value': value
    };
  }

  
  /**
   * Add value to column
   *
   * @param {number} value
   * @param {number} index
   * @returns {number}
   * @memberof GridService
   */
  public add(value: number, index: number): number
  {
    this.columns[index] += value;
    this.subject.next(this.columns);
    return this.columns[index];
  }

  /**
   * Subscribe to service
   *
   * @param {(value: number[]) => void} [next]
   * @param {(error: any) => void} [error]
   * @returns {Subscription}
   * @memberof GridService
   */
  public subscribe(
    next?: (value: number[]) => void, 
    error?: (error?: Error) => void
  ): Subscription
  {
    return this.subject.asObservable().subscribe(next, error);
  }
}
