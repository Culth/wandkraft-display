import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { StateType } from '../types';

/**
 * @export
 * @class InactiveService
 */
@Injectable({
  providedIn: 'root'
})
export class InactiveService 
{
  /**
   * @private
   * @memberof InactiveService
   */
  private subject = new Subject<StateType>();

  /**
   * @private
   * @memberof InactiveService
   */
  private timer = 120;

  /**
   * @private
   * @type {*}
   * @memberof InactiveService
   */
  private timeout: any;

  /**
   * @param {StateType} [state='active']
   * @memberof InactiveService
   */
  public refresh(state: StateType = 'active'): void
  {
    clearTimeout(this.timeout);
    this.subject.next(state);
    this.timeout = setTimeout(() => {
      if (state === 'active') {
        this.subject.next('inactive');
      }
    }, this.timer * 1000);
  }


  /**
   * Subscribe to service
   *
   * @param {(value: StateType) => void} [next]
   * @param {(error?: Error) => void} [error]
   * @returns {Subscription}
   * @memberof InactiveService
   */
  public subscribe(
    next?: (value: StateType) => void, 
    error?: (error?: Error) => void
  ): Subscription
  {
    return this.subject.asObservable().subscribe(next, error);
  }
}
