import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { InactiveService } from './inactive.service';
import { ScrollEvent } from '../classes';

/**
 * @export
 * @class ScrollService
 */
@Injectable({
  providedIn: 'root'
})
export class ScrollService 
{
  /**
   * @type {number}
   * @memberof ScrollService
   */
  public position: number = 0;

  /**
   * @private
   * @memberof ScrollService
   */
  private subject = new Subject<ScrollEvent>();

  /**
   * @private
   * @type {boolean}
   * @memberof ScrollService
   */
  private autoscroll: boolean = false;

  /**
   * @private
   * @type {*}
   * @memberof ScrollService
   */
  private autoscrollInterval: any;

  /**
   * @private
   * @type {number}
   * @memberof ScrollService
   */
  private autoscrollIntervalTimer: number = 500;

  /**
   * @private
   * @type {number}
   * @memberof ScrollService
   */
  private autoscrollIncrement: number = 0.2;


  /**
   * Creates an instance of ScrollService.
   * 
   * @param {InactiveService} inactiveService
   * @memberof ScrollService
   */
  constructor(
    private inactiveService: InactiveService
  ) { }

  /**
   * Increment Scroll Position
   *
   * @private
   * @param {number} [increment=1]
   * @memberof ScrollService
   */
  private incrementPos(increment: number = 1)
  {
    if (this.autoscroll) {
      const nextPos = this.position + increment;
      let scrollEvent: ScrollEvent;
    
      if (nextPos > 100) {
        scrollEvent = new ScrollEvent(0, false);
      } else {
        const timing = `${this.autoscrollIntervalTimer}ms`;
        scrollEvent = new ScrollEvent(nextPos, true, timing, 'linear');
      }      

      this._setPos(scrollEvent);
    }
  }

  /**
   * Set Scroll Position
   *
   * @private
   * @param {ScrollEvent} scrollEvent
   * @returns {this}
   * @memberof ScrollService
   */
  private _setPos(scrollEvent: ScrollEvent): this 
  {
    if (scrollEvent.position > 100) {
      scrollEvent.position = 100;
    } else if (scrollEvent.position < 0) {
      scrollEvent.position = 0;
    }

    this.position = scrollEvent.position;
    this.subject.next(scrollEvent);

    return this;
  }

  
  /**
   * Set Scroll Position
   *
   * @param {ScrollEvent} scrollEvent
   * @returns {this}
   * @memberof ScrollService
   */
  public setPos(scrollEvent: ScrollEvent): this
  {
    this.inactiveService.refresh();
    return this._setPos(scrollEvent);
  }

  /**
   * Get Scroll Position
   *
   * @returns {number}
   * @memberof ScrollService
   */
  public getPos(): number
  {
    return this.position;
  }
 
  /**
   * Subscribe to service
   *
   * @param {(value: ScrollEvent) => void} [next]
   * @param {(error: any) => void} [error]
   * @returns {Subscription}
   * @memberof ScrollService
   */
  public subscribe(
    next?: (value: ScrollEvent) => void, 
    error?: (error: any) => void
  ): Subscription {
    return this.subject.asObservable().subscribe(next, error);
  }
}
