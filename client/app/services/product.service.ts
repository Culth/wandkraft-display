import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ProductInterface } from '../interfaces/product.interface';
import { environment } from '../../environments/environment';

/**
 * @export
 * @class ProductService
 */
@Injectable({
  providedIn: 'root'
})
export class ProductService 
{
  /**
   * @type {string}
   * @memberof ProductService
   */
  public baseUrl: string = environment.baseUrl;
  
  /**
   * Creates an instance of ProductService.

   * @param {HttpClient} http
   * @memberof ProductService
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get Product by id
   *
   * @param {number} id
   * @param {(value: ProductInterface) => void} [next]
   * @param {(error?: Error) => void} [error]
   * @returns {Subscription}
   * @memberof ProductService
   */
  public get(
    id: number, 
    next?: (value: ProductInterface) => void, 
    error?: (error?: Error) => void
  ): Subscription
  {
    return this.http.get<ProductInterface>(`${this.baseUrl}/api/product/get/${id}`).subscribe(next, error);
  }

  /**
   * Get Product List
   *
   * @param {(value: ProductInterface[]) => void} [next]
   * @param {(error?: Error) => void} [error]
   * @returns {Subscription}
   * @memberof ProductService
   */
  public getList(
    next?: (value: ProductInterface[]) => void, 
    error?: (error?: Error) => void
  ): Subscription
  {
    return this.http.get<ProductInterface[]>(`${this.baseUrl}/api/product/list`).subscribe(next, error);
  }
}
