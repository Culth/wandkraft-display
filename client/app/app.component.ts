import { Component, OnInit } from '@angular/core';
import { InactiveService } from './services';

/**
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-root',
  template: '<display-product-grid></display-product-grid><router-outlet></router-outlet><display-instructions></display-instructions>'
})
export class AppComponent implements OnInit 
{

  /**
   * Creates an instance of AppComponent.
   * @param {InactiveService} inactiveService
   * @memberof AppComponent
   */
  constructor(
    private inactiveService: InactiveService
  ) {}

  /**
   * On Component Init
   *
   * @memberof AppComponent
   */
  public ngOnInit() {
    this.inactiveService.refresh('inactive');
  }
}
