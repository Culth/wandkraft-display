import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { environment } from '../../../environments/environment';
import { ProductInterface } from '../../interfaces';
import { ProductService, InactiveService } from '../../services';
import { StateType } from '../../types';

/**
 * @export
 * @class ProductComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.styl'],
  animations: [
    trigger('productStateAnimation', [
      state('*', style({
        opacity: 1
      })),
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', [
        animate('0.25s ease-in-out')
      ])
    ])
  ]
})
export class ProductComponent implements OnInit 
{
  /**
   * @type {ProductInterface}
   * @memberof ProductComponent
   */
  public product: ProductInterface;

  /**
   * @type {ProductInterface}
   * @memberof ProductComponent
   */
  public configProduct: ProductInterface;

  /**
   * @type {number[]}
   * @memberof ProductComponent
   */
  public selection: number[];

  /**
   * @type {string}
   * @memberof ProductComponent
   */
  public baseUrl: string = environment.baseUrl;

  /**
   * @type {StateType}
   * @memberof ProductComponent
   */
  public overlayState: StateType = 'inactive';

  /**
   * Creates an instance of ProductComponent.
   * 
   * @param {ActivatedRoute} activatedRoute
   * @param {Router} router
   * @param {ProductService} productService
   * @param {InactiveService} inactiveService
   * @memberof ProductComponent
   */
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private inactiveService: InactiveService
  ) { }

  /**
   * On Component Init
   *
   * @memberof ProductComponent
   */
  public ngOnInit(): void
  {
    this.inactiveService.refresh();

    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));

    // Get Base Product by route id
    this.productService.get(id, (result: ProductInterface) => {
      this.product = result;
    });

    // Subscribe to inactive service and go back to home
    this.inactiveService.subscribe((inactiveState: StateType) => {
      if (inactiveState === 'inactive') {
        this.router.navigateByUrl('/');
      }
    });
  }

  /**
   * Set configured product
   *
   * @param {number[]} selection
   * @memberof ProductComponent
   */
  public setConfigProduct(selection: number[]): void
  {
    this.productService.get(selection[0], (result: ProductInterface) => {
      this.configProduct = result;
      this.selection = selection;
    });
  }

  /**
   * Check if only one product is remaing
   *
   * @returns {boolean}
   * @memberof ProductComponent
   */
  public isAddToCartAvailable(): boolean
  {
    return this.selection && this.selection.length === 1;
  }

  /**
   * Toogle QR Code overlay
   *
   * @param {StateType} [state='active']
   * @memberof ProductComponent
   */
  public toggleOverlay(overlayState: StateType = 'active'): void {
    this.overlayState = overlayState;
  }
}
