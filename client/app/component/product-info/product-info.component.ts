import { Component, Input } from '@angular/core';
import { ProductInterface } from '../../interfaces';
import { ProductAttributesComponent } from './../product-attributes/product-attributes.component';
import { ProductAttributeInterface } from '../../interfaces';

/**
 * @export
 * @class ProductInfoComponent
 * @extends {ProductAttributesComponent}
 */
@Component({
  selector: 'display-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.styl']
})
export class ProductInfoComponent extends ProductAttributesComponent
{
  /**
   * @type {ProductInterface}
   * @memberof ProductInfoComponent
   */
  @Input() configProduct: ProductInterface;

  /**
   * Get all attributes with only one option
   *
   * @returns {{ [key: number]: ProductAttributeInterface }}
   * @memberof ProductInfoComponent
   */
  public getFixedAttributes(): { [key: number]: ProductAttributeInterface }
  {
    const result: { [key: number]: ProductAttributeInterface } = {};
    const attributes = this.getAttributes();

    for (const k in attributes) {
      const attribute = attributes[k];

      if (Object.keys(attribute.availableOptions).length === 1) {
        result[k] = attribute;
      }
    }

    return result;
  }

  /**
   *
   * @param configProduct
   * @param attributeId
   */
  public getOptionLabelByConfigProduct(configProduct: ProductInterface, attributeId: string|number): string {
    // @ts-ignore
    const optionId = this.product.configurations.products[configProduct.id][attributeId];
    // @ts-ignore
    return this.product.configurations.attributes[attributeId].availableOptions[optionId].label;
  }
}
