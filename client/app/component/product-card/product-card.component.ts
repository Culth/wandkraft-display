import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ProductInterface } from '../../interfaces';
import { GridService, InactiveService } from '../../services';

/**
 * @export
 * @class ProductCardComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.styl']
})
export class ProductCardComponent implements OnInit 
{
  /**
   * @type {string}
   * @memberof ProductCardComponent
   */
  public baseUrl: string = environment.baseUrl;

  /**
   * @type {number}
   * @memberof ProductCardComponent
   */
  @Input() public index: number;

  /**
   * @type {ProductInterface}
   * @memberof ProductCardComponent
   */
  @Input() public product: ProductInterface;

  /**
   * @type {number}
   * @memberof ProductCardComponent
   */
  public width: number;

  /**
   * @type {number}
   * @memberof ProductCardComponent
   */
  public height: number;

  /**
   * @type {number}
   * @memberof ProductCardComponent
   */
  public posX: number;

  /**
   * @type {number}
   * @memberof ProductCardComponent
   */
  public posY: number;

  /**
   * @type {boolean}
   * @memberof ProductCardComponent
   */
  public initialized: boolean = false;
  
  /**
   * Creates an instance of ProductCardComponent.
   * 
   * @param {GridService} gridService
   * @param {InactiveService} inactiveService
   * @param {Router} router
   * @memberof ProductCardComponent
   */
  constructor(
    private gridService: GridService,
    private inactiveService: InactiveService,
    private router: Router
  ) {}
 
  /**
   * Initilialize component
   *
   * @memberof ProductCardComponent
   */
  public ngOnInit(): void
  {
    this.width = this.gridService.getColumnWidth();
  }

  /**
   * Navigate to product
   * 
   * @param $event 
   */
  public tap($event: any): void
  {
    this.inactiveService.refresh();
    this.router.navigateByUrl(`/product/${this.product.id}`);
  }

  /**
   * Initializa card
   *
   * @param {*} $event
   * @memberof ProductCardComponent
   */
  public initialize($event: any): void
  {
      const e = $event.target;
      const min = this.gridService.getMin();
      this.height = e.offsetHeight;
      this.posY = min.value;
      this.posX = this.width * min.index;
      this.gridService.add(this.height + 25, min.index);
      this.initialized = true;
  }
}