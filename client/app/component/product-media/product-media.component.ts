import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { environment } from '../../../environments/environment';
import { StateType } from '../../types';
import { ProductInterface } from '../../interfaces';

/**
 * @export
 * @class ProductMediaComponent
 */
@Component({
  selector: 'display-product-media',
  templateUrl: './product-media.component.html',
  styleUrls: ['./product-media.component.styl'],
  animations: [
    trigger('imageStateAnimation', [
      state('active', style({
        opacity: 1
      })),
      state('*', style({
        opacity: 0
      })),
      transition('active <=> *', [
        animate('0.2s ease-in-out')
      ])
    ])
  ]
})
export class ProductMediaComponent 
{ 
  /**
   * @type {string}
   * @memberof ProductMediaComponent
   */
  public baseUrl: string = environment.baseUrl;

  @Input() public product: ProductInterface;
}
