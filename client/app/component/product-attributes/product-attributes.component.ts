import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ProductInterface, ProductAttributeInterface, ProductAttributeOptionInterface } from '../../interfaces';

/**
 * @export
 * @class ProductAttributesComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-product-attributes',
  templateUrl: './product-attributes.component.html',
  styleUrls: ['./product-attributes.component.styl']
})
export class ProductAttributesComponent implements OnInit 
{
  /**
   * @type {ProductInterface}
   * @memberof ProductAttributesComponent
   */
  @Input() public product: ProductInterface;

  /**
   * @type {EventEmitter<number[]>}
   * @memberof ProductAttributesComponent
   */
  @Output() public selection: EventEmitter<number[]> = new EventEmitter<number[]>();

  /**
   * @type {string}
   * @memberof ProductAttributesComponent
   */
  public baseUrl: string = environment.baseUrl;

  /**
   * @type {{ [key: number]: number }}
   * @memberof ProductAttributesComponent
   */
  public selectedOptions: { [key: number]: number } = {};

  /**
   * On Component Init
   * 
   * @memberof ProductAttributesComponent
   */
  public ngOnInit(): void 
  {
    this.selection.emit(this.getSelectedProducts(this.selectedOptions));
  }

  /**
   * Get Attributes
   * 
   * @returns {{ [key: number]: ProductAttributeInterface }}
   * @memberof ProductAttributesComponent
   */
  public getAttributes(): { [key: number]: ProductAttributeInterface }
  {
    const result: { [key: number]: ProductAttributeInterface } = {};

    // Return empty if no configurations
    if (!this.product.configurations) {
      return result;
    }

    const configurations = this.product.configurations.products;

    // Foreach configurable product ...
    for (const productId in configurations) {
      if (configurations.hasOwnProperty(productId)) {
        const attributes = configurations[productId];

        // ... foreach attribute the product has ...
        for (const attributeId in attributes) {
          if (attributes.hasOwnProperty(attributeId)) {
            const optionId = attributes[attributeId];

            // ... add attribute to result ...
            if (!result[attributeId]) {
              result[attributeId] = this.getAttribute(parseInt(attributeId));
              result[attributeId].availableOptions = {};
            }

            // ... and options which are available.
            result[attributeId].availableOptions[optionId] = this.getOption(parseInt(attributeId), optionId);
          }
        }
      }
    }

    return result;
  }

  /**
   * Filter Attributes to get the ones which are customizable
   * 
   * @returns {{ [key: number]: ProductAttributeInterface }}
   * @memberof ProductAttributesComponent
   */
  public getCustomizableAttributes(): { [key: number]: ProductAttributeInterface }
  {
    const result: { [key: number]: ProductAttributeInterface } = {};
    const attributes = this.getAttributes();

    for (const k in attributes) {
      const attribute = attributes[k];

      if (Object.keys(attribute.availableOptions).length > 1) {
        result[k] = attribute;
      }
    }

    return result;
  }

  /**
   * Ge Attribute by id
   * 
   * @param {number} attributeId
   * @returns {ProductAttributeInterface}
   * @memberof ProductAttributesComponent
   */
  public getAttribute(attributeId: number): ProductAttributeInterface
  {
    return this.product.configurations.attributes[attributeId];
  }

  /**
   * Get Option by attribute and option id
   * 
   * @param {number} attributeId
   * @param {number} optionId
   * @returns {ProductAttributeOptionInterface}
   * @memberof ProductAttributesComponent
   */
  public getOption(attributeId: number, optionId: number): ProductAttributeOptionInterface
  {
    return {
      id: optionId,
      label: this.product.configurations.attributes[attributeId].options[optionId],
      availableProducts: this.getAvailableProductsForOption(attributeId, optionId)
    };
  }

  /**
   * Get Label of selected option by attribute id
   * 
   * @param {number} attributeId
   * @returns {(string | false)}
   * @memberof ProductAttributesComponent
   */
  public getSelectedOptionLabel(attributeId:  number): string | false
  {
    return this.selectedOptions[attributeId] ?
      this.product.configurations.attributes[attributeId].options[this.selectedOptions[attributeId]] : false;
  }

  /**
   * Get available products for option
   * 
   * @param {number} attributeId
   * @param {number} optionId
   * @returns {number[]}
   * @memberof ProductAttributesComponent
   */
  public getAvailableProductsForOption(attributeId: number, optionId: number): number[]
  {
    const result = [];

    const products = this.product.configurations.products;

    for (const k in products) {
      if (products.hasOwnProperty(k)) {
        if (products[k][attributeId] && products[k][attributeId] === optionId) {
          result.push(parseInt(k));
        }
      }
    }

    return result;
  }

  /**
   * Check if option is selected
   * 
   * @param {number} attributeId
   * @param {number} optionId
   * @returns {boolean}
   * @memberof ProductAttributesComponent
   */
  public isOptionSelected(attributeId: number, optionId: number): boolean 
  {
    return this.selectedOptions[attributeId] === optionId;
  }

  /**
   * Check if options is available
   * 
   * @param {number} attributeId
   * @param {number} optionId
   * @returns {boolean}
   * @memberof ProductAttributesComponent
   */
  public isOptionAvailable(attributeId: number, optionId: number): boolean
  {
    const selected = this.selectedOptions;
    const result = Object.create(selected);
    result[attributeId] = optionId;

    return this.getSelectedProducts(result).length > 0;
  }

  /**
   * Set Option
   *
   * @param {number} attributeId
   * @param {number} optionId
   * @memberof ProductAttributesComponent
   */
  public setOption(attributeId: number, optionId: number): void
  {
    if (this.isOptionAvailable(attributeId, optionId)) {
      if (!this.isOptionSelected(attributeId, optionId)) {
        this.selectedOptions[attributeId] = optionId;
      } else {
        delete this.selectedOptions[attributeId];
      }

      this.selection.emit(this.getSelectedProducts());
    }
  }

  /**
   * Get selected products by current or given state
   *
   * @param {*} [selected=null]
   * @returns {number[]}
   * @memberof ProductAttributesComponent
   */
  public getSelectedProducts(selected: { [key: number]: number } = null): number[]
  {
    const result: number[] = [];
    const products = this.product.configurations.products;

    if (!selected) {
      selected = this.selectedOptions;
    }

    // Foreach product ...
    for (const productId in products) {
      const product: any = products[productId];
      let isInSelection = true;

      // .. foreach attribute in selected state ...
      for (const attributeId in selected) {

        // ... check if product attribute is the same as selected state attribte.
        if (!product[attributeId] || product[attributeId] !== selected[attributeId]) {
          isInSelection = false;
        }
      }
      
      if (isInSelection) {
        result.push(parseInt(productId));
      }
    }

    return result;
  }
}
