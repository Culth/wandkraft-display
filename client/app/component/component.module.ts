import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  ProductCardComponent,
  ProductGridComponent,
  ScrollbarComponent,
  ProductAttributesComponent,
  ProductInfoComponent,
  ProductMediaComponent,
  ProductOptionComponent,
  ProductOverlayComponent,
  InstructionsComponent,
  ProductBadgesComponent
} from './';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    ProductCardComponent,
    ProductGridComponent,
    ScrollbarComponent,
    ProductAttributesComponent,
    ProductInfoComponent,
    ProductMediaComponent,
    ProductOptionComponent,
    ProductOverlayComponent,
    ProductBadgesComponent,
    InstructionsComponent
  ],
  exports: [
    ProductCardComponent,
    ProductGridComponent,
    ScrollbarComponent,
    ProductAttributesComponent,
    ProductInfoComponent,
    ProductMediaComponent,
    ProductOptionComponent,
    ProductOverlayComponent,
    ProductBadgesComponent,
    InstructionsComponent
  ]
})
export class ComponentModule { }
