import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ProductInterface } from '../../interfaces';
import { GridService, ScrollService, ProductService } from '../../services';
import { ScrollEvent } from '../../classes';

/**
 * @export
 * @class ProductGridComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.styl']
})
export class ProductGridComponent implements OnInit 
{
  /**
   * @type {string}
   * @memberof ProductGridComponent
   */
  public baseUrl: string = environment.baseUrl;
  
  
  /**
   * @type {ProductInterface[]}
   * @memberof ProductGridComponent
   */
  public products: ProductInterface[] = [];
  
  /**
   * @type {number}
   * @memberof ProductGridComponent
   */
  public height: number = 0;

  /**
   * @type {number}
   * @memberof ProductGridComponent
   */
  public offset: number = 0;

  /**
   * @type {string}
   * @memberof ProductGridComponent
   */
  public transition: string = '';


  /**
   * @type {number}
   * @memberof ProductGridComponent
   */
  public panDist: number = 0;

  /**
   *C reates an instance of ProductGridComponent.
   * @param {GridService} gridService
   * @param {ProductService} productService
   * @param {ScrollService} scrollService
   * @memberof ProductGridComponent
   */
  constructor(
    private gridService: GridService,
    private productService: ProductService,
    private scrollService: ScrollService
  ) {}

  /**
   * @memberof ProductGridComponent
   */
  public ngOnInit() 
  {
    // Reset Columns
    this.gridService.clear();
    
    // Get Product List
    this.productService.getList(
      (result: ProductInterface[]) => {
        this.products = result;
      },
      (error: Error) => {
        this.products = [];
      }
    );

    // Subscribe to grid service and set height of wrapper
    this.gridService.subscribe(() => {
      const max = this.gridService.getMax();
      if (max.value !== this.height) {
        this.height = max.value;
      }
    });
    
    // Subscribe to scrollbar, set scroll position and scroll transition
    this.scrollService.subscribe((scrollEvent: ScrollEvent) => {
      if (scrollEvent.transition) {
        const transition = `top ${scrollEvent.transitionDuration} ${scrollEvent.transitionEase}`;

        if (this.transition !== transition) {
          this.transition = transition;
        }
      } else if (this.transition) {
        this.transition = '';
      }

      this.offset = ((this.height - window.innerHeight) * (scrollEvent.position / 100)) * -1;
    });
  }

  /**
   * On Grid Pan
   * 
   * @param $event 
   */
  public pan($event: any)
  {
    const pos = this.scrollService.getPos();
    const dist = $event.deltaY - this.panDist;
    const distPercente = ((dist / this.height * 100) * -1);
    const destination = pos + distPercente;

    const scrollEvent = new ScrollEvent(destination,  false, '', '');

    this.scrollService.setPos(scrollEvent);
    this.panDist = $event.deltaY;
    if ($event.isFinal) {
      this.panDist = 0;
    }
  }
}
