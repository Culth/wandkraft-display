import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { environment } from '../../../environments/environment';
import { InactiveService } from '../../services';
import { StateType } from '../../types';
import { ProductInterface, ProductAttributeInterface } from '../../interfaces';

@Component({
  selector: 'display-product-overlay',
  templateUrl: './product-overlay.component.html',
  styleUrls: ['./product-overlay.component.styl']
})
export class ProductOverlayComponent {
  public baseUrl: string = environment.baseUrl;

  @Input() public state: StateType = 'active';

  @Input() public product: ProductInterface;

  @Input() public parentProduct: ProductInterface;

  @Output() public stateChange = new EventEmitter<StateType>();

  public mode: 'select'|'qr-code'|'card' = 'select';

  public useProductCard: boolean = environment.useProductCards;

  protected fieldGroups = [
    {
      'attributes': [
        'format',
        'variant'
      ],
    },
    {
      'attributes': [
        'lifestyle_variant'
      ]
    }
  ];

  constructor(
    private inactiveService: InactiveService
  ) { }

  public open(): void 
  {
    this._setState('active');
  }

  public close(event: any = null, element: any = null): void 
  {
    if ((event && element) && (event.target !== element)) {
      return;
    }
    this.setModeSelect();
    this._setState('inactive');
  }

  private _setState(inactiveState: StateType = 'inactive'): void
  {
    this.inactiveService.refresh();
    this.stateChange.emit(inactiveState);
  }

  public setMode(mode: 'select'|'qr-code'|'card'): void 
  {
    this.inactiveService.refresh();
    this.mode = mode;
  }

  public setModeQrCode(): void 
  {
    this.setMode('qr-code');
  }

  public setModeSelect(): void
  {
    this.setMode('select');
  }

  public setModeCard(): void
  {
    this.setMode('card');
  }

  public getAttributeIdByCode(code: string): number
  {
    const attributes = this.parentProduct.configurations.attributes;
    let id: number;

    for (const k in attributes) {
      if (attributes.hasOwnProperty(k) && attributes[k].code === code) {
        id = parseInt(k);
        break;
      }
    }

    return id;
  }

  public getAttributeByCode(code: string): ProductAttributeInterface
  {
    const id: number = this.getAttributeIdByCode(code);
    
    return this.getAttribute(id);
  }

  public getAttribute(id: number): ProductAttributeInterface
  {
    return this.parentProduct.configurations.attributes[id];
  }

  public getSelectedAttributeSet()
  {
    return this.parentProduct.configurations.products[this.product.id];
  }

  public isChecked(attributeKey: string, optionKey: number): boolean
  {
    const id = this.getAttributeIdByCode(attributeKey);
    return this.getSelectedAttributeSet()[id] === optionKey;
  }
}
