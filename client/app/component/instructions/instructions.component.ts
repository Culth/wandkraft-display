import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { StateType } from '../../types';
import { InactiveService } from '../../services';

/**
 * @export
 * @class InstructionsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.styl'],
  animations: [
    trigger('stateSwitchAnimation', [
      state('active', style({
        opacity: 0,
        'pointer-events': 'none'
      })),
      state('*', style({
        opacity: 1
      })),
      transition('active <=> *', [
        animate('0.2s ease-in-out')
      ])
    ])
  ]
})
export class InstructionsComponent implements OnInit 
{  
  /**
   * Instructions State
   * @type {StateType}
   * @memberof InstructionsComponent
   */
  public state: StateType = 'inactive';


  /**
   * Creates an instance of InstructionsComponent.
   * @param {InactiveService} inactiveService
   * @memberof InstructionsComponent
   */
  constructor(
    private inactiveService: InactiveService
  ) { }

  /**
   * On Init
   * @memberof InstructionsComponent
   */
  public ngOnInit(): void 
  {
    this.inactiveService.subscribe((inactiveState: StateType) => {
      this.state = inactiveState;
    });
  }

  /**
   * continue interaction
   * @memberof InstructionsComponent
   */
  public continue(): void 
  {
    this.inactiveService.refresh();
  }
}
