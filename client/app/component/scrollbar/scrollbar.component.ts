import { Component, OnInit, ElementRef } from '@angular/core';
import { ScrollService } from '../../services';
import { ScrollEvent } from '../../classes';

/**
 * @export
 * @class ScrollbarComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-scrollbar',
  templateUrl: './scrollbar.component.html',
  styleUrls: ['./scrollbar.component.styl']
})
export class ScrollbarComponent implements OnInit 
{ 
  /**
   * @type {number}
   * @memberof ScrollbarComponent
   */
  public pos: number = 0;


  /**
   * @type {*}
   * @memberof ScrollbarComponent
   */
  public bounding: any;

  /**
   * @type {string}
   * @memberof ScrollbarComponent
   */
  public transition: string = '';

  /**
   * Constructor
   * 
   * @param scrollService 
   * @param elementRef 
   */
  constructor(
    private scrollService: ScrollService,
    private elementRef: ElementRef
  ) { }


  /**
   * @memberof ScrollbarComponent
   */
  public ngOnInit() 
  {
    this.scrollService.subscribe((scrollEvent: ScrollEvent) => {
      this.pos = scrollEvent.position;
      if (scrollEvent.transition) {
        const transition = `height ${scrollEvent.transitionDuration} ${scrollEvent.transitionEase}`;
        if (this.transition !== transition) {
          this.transition = transition;
        }
      } else if (this.transition) {
        this.transition = '';
      }
    });

    this.bounding = this.elementRef.nativeElement.getBoundingClientRect();
  }

  /**
   * Set pos on pan
   * 
   * @param $event 
   */
  public pan($event: any)
  {
    const height = this.bounding.height;
    const elY = this.bounding.y;
    const clickY = $event.center.y;
    const pos = ((clickY - elY) / height) * 100;
    const scrollEvent = new ScrollEvent(pos, false);

    this.scrollService.setPos(scrollEvent);
  }

  /**
   * Set pos on tap
   * 
   * @param $event 
   */
  public tap($event: any)
  {
    const height = this.bounding.height;
    const elY = this.bounding.y;
    const clickY = $event.center.y;
    const pos = ((clickY - elY) / height) * 100;
    const scrollEvent = new ScrollEvent(pos);
    
    this.scrollService.setPos(scrollEvent);
  }
}
