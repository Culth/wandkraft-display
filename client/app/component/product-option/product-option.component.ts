import { Component, Input, Output, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ProductAttributeInterface } from '../../interfaces';
import { InactiveService } from '../../services';

/**
 * @export
 * @class ProductOptionComponent
 */
@Component({
  selector: 'display-product-option',
  templateUrl: './product-option.component.html',
  styleUrls: ['./product-option.component.styl']
})
export class ProductOptionComponent
{
  /**
   * @type {string}
   * @memberof ProductOptionComponent
   */
  public baseUrl: string = environment.baseUrl;

  /**
   * @type {number}
   * @memberof ProductOptionComponent
   */
  @Input() public optionId: number;

  /**
   * @type {string}
   * @memberof ProductOptionComponent
   */
  @Input() public option: string;

  /**
   * @type {number}
   * @memberof ProductOptionComponent
   */
  @Input() public attributeId: number;

  /**
   * @type {ProductAttributeInterface}
   * @memberof ProductOptionComponent
   */
  @Input() public attribute: ProductAttributeInterface;

  /**
   * @type {boolean}
   * @memberof ProductOptionComponent
   */
  @Input() public isSelected: boolean = false;
  
  /**
   * @type {boolean}
   * @memberof ProductOptionComponent
   */
  @Input() public isAvailable: boolean = true;
  
  /**
   * @type {{[key: number]: string}}
   * @memberof ProductOptionComponent
   */
  @Input() public swatchImages?: {[key: number]: string};
  
  /**
   * @memberof ProductOptionComponent
   */
  @Output() public select = new EventEmitter<{
    attributeId: number;
    optionId: number;
  }>();

  /**
   * Creates an instance of ProductOptionComponent.
   * TODO: fix event loop initialization; Refreshes at every state change
   * @param {InactiveService} inactiveService
   * @memberof ProductOptionComponent
   */
  constructor(
    private inactiveService: InactiveService
  ) {}

  /**
   * Set option and emit select event
   *
   * @memberof ProductOptionComponent
   */
  public setOption(): void
  {
    this.inactiveService.refresh();

    this.select.emit({
      attributeId: this.attributeId,
      optionId: this.optionId
    });
  }

  /**
   * Get Watch Image
   *
   * @returns {string}
   * @memberof ProductOptionComponent
   */
  public getSwatchImage(): string 
  {
      return this.baseUrl + this.attribute.swatch_images[this.optionId].path;
  }
}
