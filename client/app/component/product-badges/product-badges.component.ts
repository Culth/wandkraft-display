import { Component, Input } from '@angular/core';
import { ProductInterface } from '../../interfaces';

/**
 * @export
 * @class ProductCardComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'display-product-badges',
  templateUrl: './product-badges.component.html',
  styleUrls: ['./product-badges.component.styl']
})
export class ProductBadgesComponent {

  /**
   * @type {ProductInterface}
   * @memberof ProductCardComponent
   */
  @Input() public product: ProductInterface;
}