import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import * as Hammer from 'hammerjs';
import { ComponentModule } from './component/component.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent, ProductComponent } from './routes';
import { ProductService, GridService, InactiveService, ScrollService } from './services';

export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'pinch': {
      enable: false
    },
    'rotate': {
      enable: false
    },
    'swipe': { 
      direction: Hammer.DIRECTION_ALL
    },
    'pan': {
      direction: Hammer.DIRECTION_VERTICAL,
      threshold: 5
    }
  };
}

/**
 * @export
 * @class AppModule
 */
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ComponentModule,
    BrowserAnimationsModule
  ],
  providers: [
    HttpModule,
    ProductService,
    GridService,
    ScrollService,
    InactiveService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
