import { ScrollEventInterface } from '../interfaces';

/**
 * @export
 * @class ScrollEvent
 */
export class ScrollEvent {
	
	/**
	 *Creates an instance of ScrollEvent.
	 * @param {number} position
	 * @param {boolean} [transition=true]
	 * @param {string} [transitionDuration='0.5s']
	 * @param {'ease-in-out'} transitionEase
	 * @memberof ScrollEvent
	 */
	constructor(
		private _position: number = 0,
		private _transition: boolean = true,
		private _transitionDuration: string = '0.5s',
		private _transitionEase: string = 'ease-in-out'
	) {}

	
	/**
	 * @type {void}
	 * @memberof ScrollEvent
	 */
	public set position(position: number) 
	{
		if (position > 100) {
			position = 100;
		} else if (position < 0) {
			position = 0;
		}
		this._position = position;
	}

	/**
	 * @readonly
	 * @type {number}
	 * @memberof ScrollEvent
	 */
	public get position(): number 
	{
		return this._position;
	}

	/**
	 * @readonly
	 * @type {boolean}
	 * @memberof ScrollEvent
	 */
	public get transition(): boolean
	{
		return this._transition;
	}

	/**
	 * @readonly
	 * @type {string}
	 * @memberof ScrollEvent
	 */
	public get transitionDuration(): string
	{
		return this._transitionDuration;
	}

	
	/**
	 * @readonly
	 * @type {string}
	 * @memberof ScrollEvent
	 */
	public get transitionEase(): string
	{
		return this._transitionEase;
	}

	/**
	 * @readonly
	 * @type {ScrollEventInterface}
	 * @memberof ScrollEvent
	 */
	public get event(): ScrollEventInterface
	{
		return {
			position: this.position,
			transition: this.transition,
			transitionDuration: this.transitionDuration,
			transitionEase: this.transitionEase
		};
	}
}