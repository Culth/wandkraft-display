import mongoose from 'mongoose';
import { ProductInterface } from '@interface/product.interface';

export type ProductModelInterface = mongoose.Document & ProductInterface;

const productSchema = new mongoose.Schema({
	id: {type: Number, required: true},
	type: {type: String, required: true},
	sku: {type: String, required: true},
	name: {type: String, required: true},
	visibility: {type: String, required: true},
	sort_order: {type: Number, default: 0},
	category_ids: [Number],
	theme_color: {type: String, default: 'ffffff'},
	short_description: String,
	url_key: String,
	store_id: {type: Number, required: true},
	price: {type: Number, required: true},
	price_formatted: String,
	image: {type: String, required: true},
	small_image: {type: String, required: true},
	thumbnail: {type: String, required: true},
	qr_code: String,
	configurations: {
		attributes: Map,
		products: Map
	},
	badges: [{
		class: {type: String},
		label: {type: String}
	}]
});

export const Product = mongoose.model('Product', productSchema);

export default Product;
