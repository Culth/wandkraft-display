import { Product, ProductModelInterface } from '@model/product';
import { ProductInterface } from '@interface/product.interface';
import { ResourceModelInterface } from '@interface/resourceModel.interface.ts';
import { Document } from 'mongoose';


/**
 * @export
 * @param {object} where
 * @returns {Promise<ProductModelInterface>}
 */
export function getOne(where: object): Promise<ProductModelInterface> 
{
	return new Promise<ProductModelInterface>(
		(resolve: (result: ProductModelInterface) => void, reject: (error?: Error) => void) => {
			Product.findOne()
				.where(where)
				.exec()
				.then((result: ProductModelInterface) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 * @export
 * @param {object} where
 * @returns {(Promise<Document & ProductModelInterface[]>)}
 */
export function getList(where: object): Promise<Document & ProductModelInterface[]> 
{
	return new Promise<Document & ProductModelInterface[]>(
		(resolve: (result: Document & ProductModelInterface[]) => void, reject: (error?: Error) => void) => {
			Product.find()
				.where(where)
				.sort('sort_order')
				.exec()
				.then((result: Document & ProductModelInterface[]) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 *
 *
 * @export
 * @param {ProductInterface} object
 * @returns {Promise<Document>}
 */
export function createOne(object: ProductInterface): Promise<Document> 
{
	return new Promise<Document>(
		(resolve: (result: Document) => void, reject: (error?: Error) => void) => {
			Product.create(object)
				.then((result: Document) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 * @export
 * @param {ProductInterface[]} objects
 * @returns {Promise<ProductModelInterface[]>}
 */
export function createMany(objects: ProductInterface[]): Promise<ProductModelInterface[]> 
{
	return new Promise<ProductModelInterface[]>(
		(resolve: (result: ProductModelInterface[]) => void, reject: (error?: Error) => void) => {
			Product.insertMany(objects)
				.then((result: ProductModelInterface[]) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 * @export
 * @param {object} where
 * @param {ProductInterface} object
 * @param {*} [options={}]
 * @returns {Promise<ProductModelInterface>}
 */
export function updateOne( where: object, object: ProductInterface, options: any = {}): Promise<ProductModelInterface> 
{
	return new Promise<ProductModelInterface>(
		(resolve: (result: ProductModelInterface) => void, reject: (error?: Error) => void) => {
			Product.updateOne(where, object, options)
				.then((result: ProductModelInterface) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 * @export
 * @param {object} where
 * @param {ProductInterface} object
 * @returns {Promise<Document>}
 */
export function updateMany(where: object, object: ProductInterface): Promise<Document> 
{
	return new Promise<Document>(
		(resolve: (result: Document) => void, reject: (error?: Error) => void) => {
			Product.updateMany(where, object)
				.then((result: Document) => {
					resolve(result);
				})
				.catch((error: Error) => {
					reject(error);
				});
		}
	);
}

/**
 *
 *
 * @export
 * @param {object} where
 * @returns {Promise<boolean>}
 */
export function deleteOne(where: object): Promise<boolean> 
{
	return new Promise<boolean>(
		(resolve: (result: boolean) => void, reject: (error?: Error) => void) => {
			Product.remove(where, (err?: Error) => {
				if (!err) {
					resolve(true);
				}

				reject(err);
			});
		}
	);
}


/**
 *
 *
 * @export
 * @param {object} where
 * @returns {Promise<boolean>}
 */
export function deleteMany(where: object): Promise<boolean> 
{
	return new Promise<boolean>(
		(resolve: (result: boolean) => void, reject: (error?: Error) => void) => {
			Product.deleteMany(where, (err?: Error) => {
				if (!err) {
					resolve(true);
				}

				reject(err);
			});
		}
	);
}

export const ProductResource: ResourceModelInterface = {
	model: Product,
	get: getOne,
	getList: getList,
	create: createOne,
	createMany: createMany,
	update: updateOne,
	updateMany: updateMany,
	delete: deleteOne,
	deleteMany: deleteMany
};

export default ProductResource;