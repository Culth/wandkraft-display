import { LOCALE } from '@util/secrets';
import { Request, Response } from 'express';

export const sendIndex = async (req: Request, res: Response) => {
	const root = process.env.PWD;
	res.sendFile(`${root}/dist/client-${LOCALE}/index.html`);
};