import ProductResource from '@model/resourceModel/product';
import { Response, Request, NextFunction } from 'express';

/**
 * Get json product
 *
 * @param req
 * @param res
 */
export const get = async (req: Request, res: Response) => {
	const result = await ProductResource.get({id: req.params.id});
	res.json(result);
};

/**
 * Get json product list
 *
 * @param req
 * @param res
 */
export const getList = async (req: Request, res: Response) => {
	const result = await ProductResource.getList({type: 'configurable'});
	res.json(result);
};