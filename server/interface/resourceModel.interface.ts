import { Model, Document } from 'mongoose';

/**
 * @export
 * @interface ResourceModelInterface
 */
export interface ResourceModelInterface {
	model: Model<Document>;
	get?: (where: object) => Promise<Document>;
	getList?: (where: object) => Promise<Document>;
	create?: (object: any) => Promise<Document>;
	createMany?: (objects: any[]) => Promise<Document[]>;
	update?: (where: object, object: any, options: any) => Promise<Document>;
	updateMany?: (where: object, object: any) => Promise<Document>;
	delete?: (where: object) => Promise<boolean>;
	deleteMany?: (where: object) => Promise<boolean>;
}