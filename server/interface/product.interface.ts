
/**
 * @export
 * @interface ProductInterface
 */
export interface ProductInterface {
    id: number;
    type: string;
    sku: string;
    name: string;
    visibility: string;
    sort_order: number;
    category_ids: Array<number>;
    theme_color: string;
    short_description: string;
    url_key: string;
    store_id: string;
    price: number;
    price_formatted: string;
    image: string;
    small_image: string;
    thumbnail: string;
    qr_code?: string;
    configurations?: ProductConfigurationsInterface;
    badges: Array<{
        class: string,
        label: string
    }>;
}

/**
 * @export
 * @interface ProductConfigurationsInterface
 */
export interface ProductConfigurationsInterface {
    attributes: {
        [key: number]: ProductAttributeInterface
    };
    products: {
        [key: number]: {
            [key: number]: number
        }
    };
}

/**
 * @export
 * @interface ProductAttributeInterface
 */
export interface ProductAttributeInterface {
    label: string;
    code: string;
    type: string;
    options: Map<number, string>;
    swatch_images?: {
        [key: number]: {
            url: string,
            path: string
        }
    };
}
