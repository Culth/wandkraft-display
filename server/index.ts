require('module-alias/register');

import * as mongoose from '@config/mongoose';
import app from '@config/express';
import logger from '@util/logger';
import { ENVIROMENT } from '@util/secrets';

import * as productApiController from '@controller/api/product';
import * as homeController from '@controller/home';


/**
 * Start Server
 */
async function start() {
	const connection = await mongoose.connect();

	app.get('/api/product/get/:id', productApiController.get);
	app.get('/api/product/list', productApiController.getList);
	app.get('*', homeController.sendIndex);

	app.listen(app.get('port'), () => {
		logger.info('Sever running at 127.0.0.1:' + app.get('port') + ' in ' + ENVIROMENT + ' mode.');
	});
}

start();

export default app;