import * as fs from 'fs';
import path from 'path';

/**
 * Async recursive Directory creation
 *
 * @export
 * @param {string} dir
 * @returns {Promise<string>}
 */
export async function mkDirSync(dir: string): Promise<string>
{
	if (!fs.existsSync(dir) && dir) {
		const paths = dir.split(/\/|\\/);
		const rootPath = process.env.PWD;
		let currentPath = rootPath;

		for (let i = 0; i < paths.length; i++) {
			currentPath += path.sep + paths[i];

			if (!fs.existsSync(currentPath)) {
				await fs.mkdirSync(currentPath);
			}
		}
	}

	return dir;
}

/**
 * Async file writing with async recursive directory creation
 *
 * @export
 * @param {string} filepath
 * @param {string} content
 * @param {boolean} [force=false]
 * @param {string} [charset='utf8']
 * @returns {Promise<string>}
 */
export async function writeFileSync(filepath: string, content: string, force: boolean = false, charset: string = 'utf8'): Promise<string>
{
	await mkDirSync(path.dirname(filepath));
	if (!fs.existsSync(filepath) || force) {
		if (fs.existsSync(filepath) && force) {
			fs.unlinkSync(filepath);
		}
		await fs.writeFileSync(filepath, content, charset);
	}
	return filepath;
}

/**
 * Async does path exists
 *
 * @export
 * @param {string} filepath Path to file
 * @returns {Promise<boolean>}
 */
export async function existsSync(filepath: string): Promise<boolean>
{
	return await fs.existsSync(filepath);
}