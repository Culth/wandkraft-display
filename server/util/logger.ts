import { createLogger, format, transports } from 'winston';
import * as filesystem from '@util/filesystem';

filesystem.mkDirSync('server/var/log');

/**
 * Logger format
 *
 * @param type
 */
const getLoggerFormat = (type: string) => {
	return format.combine(
		format.printf((info?: any) => {
			if (!info.timestamp) {
				info.timestamp = new Date().toISOString();
			}
			info.message = `[${info.timestamp}] ${info.message}`;
			return info;
		}),
		format.printf((info?: any) => {
			info.message = `[${info.level}]${info.message}`;
			return info;
		}),
		format.splat(),
		format.printf((info?: any) => {
			return info.message;
		})
	);
};

/**
 * Error Logger
 */
export const errorLogger = createLogger({
	transports: [
		new (transports.File)(
			{
				filename: 'server/var/log/error.log',
				level: 'error',
				format: getLoggerFormat('error')
			}
		),
		new (transports.Console)(
			{
				level: 'error',
				format: format.combine(
					format.colorize(),
					getLoggerFormat('error')
				)
			}
		)
	]
});

/**
 * Warn Logger
 */
export const warnLogger = createLogger({
	transports: [
		new (transports.File)(
			{
				filename: 'server/var/log/warn.log',
				level: 'warn',
				format: getLoggerFormat('warn')
			}
			),
		new (transports.Console)(
			{
				level: 'warn',
				format: format.combine(
					format.colorize(),
					getLoggerFormat('warn')
				)
			}
		)
	]
});

/**
 * Info Logger
 */
export const infoLogger = createLogger({
	transports: [
		new (transports.File)(
			{
				filename: 'server/var/log/info.log',
				level: 'info',
				format: getLoggerFormat('info')
			}
		),
		new (transports.Console)(
			{
				level: 'info',
				format: format.combine(
					format.colorize(),
					getLoggerFormat('info')
				)
			}
		)
	]
});

/**
 * Debug logger
 */
export const debugLogger = createLogger({
	transports: [
		new (transports.File)(
			{
				filename: 'server/var/log/debug.log',
				level: 'debug',
				format: getLoggerFormat('debug')
			}
		),
		new (transports.Console)(
			{
				level: 'debug',
				format: format.combine(
					format.colorize(),
					getLoggerFormat('debug')
				)
			}
		)
	]
});

/**
 * Verbose logger
 */
export const verboseLogger = createLogger({
	transports: [
		new (transports.Console)(
			{
				level: 'verbose',
				format: format.combine(
					format.colorize(),
					getLoggerFormat('verbose')
				)
			}
		)
	]
});

/**
 * Logger composite
 */
export default {
	error: errorLogger.error,
	warn: warnLogger.warn,
	info: infoLogger.info,
	debug: debugLogger.debug,
	verbose: verboseLogger.verbose
};