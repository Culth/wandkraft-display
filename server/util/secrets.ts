import logger from '@util/logger';
import dotenv from 'dotenv';
import fs from 'fs';

if (fs.existsSync('.env')) {
	dotenv.config({path: '.env'});
} else {
	logger.error('.env file is not defined');
	process.exit(1);
}

export const ENVIROMENT = process.env['NODE_ENV'];
export const MONGODB_URI = process.env['MONGODB_URI'];
export const WK_API_REQUEST_URI = process.env['WK_API_REQUEST_URI'];
export const WK_API_KEY = process.env['WK_API_KEY'];
export const WK_ADDRESS_ID = process.env['WK_ADDRESS_ID'];
export const LOCALE = process.env['LOCALE'] ? process.env['LOCALE'] : 'nl';
if (!MONGODB_URI) {
	logger.error('Mongo connection uri is not configured.');
	process.exit(1);
}