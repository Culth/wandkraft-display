import logger from '@util/logger';
import { Request, Response, NextFunction } from 'express';

/**
 * Log incoming request to console
 *
 * @param request
 * @param response
 * @param nex
 */
export const requestLoggerMiddleware = (request: Request, response: Response, next: NextFunction) => {
	const ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
	const path = request.path;
	logger.verbose('\'%s\' requested from [%s]', path, ip);
	next();
};

/**
 * Open up cross origin requests from port 4200. Primarly used in development.
 *
 * @param request
 * @param response
 * @param next
 */
export const corsMiddleware = (request: Request, response: Response, next: NextFunction) => {
	response.header('Access-Control-Allow-Origin', 'http://localhost:4200');
	next();
};