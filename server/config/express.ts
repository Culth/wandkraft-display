import express from 'express';
import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import { ENVIROMENT, LOCALE } from '@util/secrets';
import * as middleware from '@config/middleware';

const app = express();

app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(`dist/client-${LOCALE}`, { maxAge: 31557600000}));
app.use(express.static('server/public', { maxAge: 31557600000}));

app.use(middleware.requestLoggerMiddleware);
app.use(middleware.corsMiddleware);

if (ENVIROMENT !== 'production') {
	app.use(errorHandler());
}

export default app;