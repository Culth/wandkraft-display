import mongoose, { Mongoose } from 'mongoose';
import bluebird from 'bluebird';
import logger from '@util/logger';
import { MONGODB_URI } from '@util/secrets';

(<any>mongoose).promise = bluebird;

/**
 * Get Mongoose Connection
 *
 * @export
 * @returns {Promise<Mongoose>}
 */
export function connect(): Promise<Mongoose> {
	return mongoose.connect(MONGODB_URI, { useNewUrlParser: true });
}