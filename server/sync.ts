require('module-alias/register');

import * as requestPromise from 'request-promise';
import * as mongoose from '@config/mongoose';
import logger from '@util/logger';
import * as filesystem from '@util/filesystem';
import { WK_API_REQUEST_URI, WK_API_KEY, WK_ADDRESS_ID, LOCALE } from '@util/secrets';
import { ProductInterface } from '@interface/product.interface';
import { ProductResource } from '@model/resourceModel/product';
import * as readline from 'readline';


/**
 * Show Process
 *
 * @param {number} current
 * @param {number} to
 * @param {string} [label='']
 * @param {number} [length=20]
 * @returns {Promise<boolean>}
 */
async function showProgress(
	current: number,
	to: number,
	label: string = '',
	length: number = 20
): Promise<boolean> {
	readline.clearLine(process.stdout, 0);
	readline.cursorTo(process.stdout, 0);

	current = current > to ? to : current;

	const percent = Math.round(current / to * 100) + '%';

	const i: number = Math.round(current / to * length);
	let progress: string = new Array(i).join('=');
	progress += new Array(length - i).join('.');

	process.stdout.write(`[${progress}] ${percent} ${label}`);
	return true;
}

/**
 * Add New Line
 *
 * @returns {Promise<void>}
 */
async function newLine(label: string = ''): Promise<void> {
	if (label) {
		readline.clearLine(process.stdout, 0);
		readline.cursorTo(process.stdout, 0);
	}
	process.stdout.write(label + '\n');
}

/**
 * Synchronize Application
 * 
 * @returns {Promise<void>}
 */
async function sync(): Promise<void> {
	await mongoose.connect();
	logger.info('Starting Synchronisation ...');
	newLine();
	let products = await requestProductList();
	products = await createStaticFiles(products);
	products = await createProducts(products);
	newLine();
	logger.info('Syncronisation finished. %d added or updated.', products.length);
	process.exit();
}

/**
 * Fetch Products
 *
 * @returns {Promise<ProductInterface[]>}
 */
async function requestProductList(): Promise<ProductInterface[]> {
	const products: ProductInterface[] = [];
	let size =  20;
	let total = 40;
	let counter: number = 0;

	for (let page = 1; page * size < total + size; page++) {
		try {
			const path = [WK_API_REQUEST_URI, LOCALE, 'rest', 'V1', 'display', WK_API_KEY, WK_ADDRESS_ID, 'products', page].join('/');
			const result = await requestPromise.get(path);
			const resultJson = JSON.parse(result);
			const resultRequest = resultJson[0];
			const resultProduct = resultJson[1];

			size = resultRequest['size'];
			total = resultRequest['total'];

			counter += resultRequest['size'];
			await showProgress(counter, resultRequest['total'], `- Fetching ${counter} from ${total}`);

			resultProduct.forEach((element: ProductInterface): void => {
				products.push(element);
			});
		} catch (error) {
			logger.error('Failed sync.' + error);
		}
	}

	newLine(`[complete] Fetched ${products.length} products`);

	return products;
}

/**
 * Get static files from host by product array
 *
 * @param products
 */
async function createStaticFiles(products: any[]): Promise<ProductInterface[]> {
	const fields = [
		{
			identififier: 'image',
			options: {encoding: 'binary'},
			charset: 'binary'
		},
		{
			identififier: 'small_image',
			options: {encoding: 'binary'},
			charset: 'binary'
		},
		{
			identififier: 'thumbnail',
			options: {encoding: 'binary'},
			charset: 'binary'
		},
		{
			identififier: 'qr_code',
			options: {},
			charset: 'utf8'
		}
	];

	let counter: number = 0;

	for (let i = 0; i < products.length; i++) {
		const product: any = products[i];

		for (let j = 0; j < fields.length; j++) {
			const field = fields[j];
			const from: string = WK_API_REQUEST_URI + product[field.identififier];
			const to: string = product[field.identififier].replace('/pub/media', 'server/public/media');

			try {
				if (!await filesystem.existsSync(to)) {
					const result = await requestPromise.get(from, field.options);
					filesystem.writeFileSync(to, result, false, field.charset);
				}
			} catch (error) {
				logger.error('Failed file transfer %s', from);
				products.splice(i, 1);
				i--;
				break;
			}

			products[i][field.identififier] = to.replace('server/public', '');
		}

		await createSwatchImages(product);

		counter++;
		if (counter % 10 === 0) {
			await showProgress(counter, products.length, `- Static assets ${counter} from ${products.length}`);
		}
	}

	newLine(`[complete] Static files for ${products.length} Products created`);

	return products;
}

/**
 * Get swatch images
 *
 * @param product
 */
async function createSwatchImages(product: ProductInterface) {
	if (product.configurations && product.configurations.attributes) {
		const attributes = product.configurations.attributes;

		for (const attributeId in attributes) {
			const attribute = attributes[attributeId];

			if (attribute.swatch_images) {
				const swatchImages = attribute.swatch_images;

				for (const optionId in swatchImages) {
					try {
						const swatchImage = swatchImages[optionId];
						const to = `server/public/media/swatch/${attributeId}/${optionId}${swatchImage.path}`;
						const requestOptions = { encoding: 'binary' };

						if (!await filesystem.existsSync(to)) {
							const result = await requestPromise.get(swatchImage.url, requestOptions);
							filesystem.writeFileSync(to, result, false, 'binary');
						}

						delete swatchImage.url;
						swatchImage.path = to.replace('server/public', '');
					} catch (error) {
						logger.error('Failed to get swatch image for attribute %d for the option %d', attributeId, optionId);
						attribute.type = 'default';
						delete attribute.swatch_images;
						break;
					}
				}
			}
		}
	}

	return product;
}

/**
 * Update or create products in database by product array
 *
 * @param products
 */
async function createProducts(products: ProductInterface[]) {
	if (products.length) {
		await ProductResource.model.deleteMany({});
	}

	let counter = 0;

	for (let i = 0; i < products.length; i++) {
		const product = products[i];
		try {
			await ProductResource.update({id: product.id}, product, {
				upsert: true,
				setDefaultOnInsert: true
			});
			counter++;

			if (counter % 10 === 0) {
				await showProgress(counter, products.length, `Database update ${counter} from ${products.length}`);
			}
		} catch (error) {
			logger.error('Cannot insert product %s', product['sku'] ? product['sku'] : JSON.stringify(product));
		}
	}

	newLine(`[complete] ${products.length} added/updated in database`);

	return products;
}

sync();