#!/bin/sh

# Help verbose  
# ------------------------------------------------------------------------------------

show_help()
{
	cat << EOM
Display setup. Make sure git is installed before you continue
	-l|--locale
		[required] Lanuage code. ('nl', 'en', 'de')
	-k|--apikey
		[required] Api Key
	-a|--address
		[required] Adress ID
	-u|--user
	--repo
		[default: https://Culth@bitbucket.org/Culth/wandkraft-display.git] Repo for project
	--branch
		[default: master] Branch of project
	--mongo
		[default: mongodb://localhost:27017/display] Mongo connect string
	--port
		[default: 80] Port
	--baseurl
		[default: https://display.wandkraft.com] Base Url
EOM
	exit 1;
}

# Variables
# ------------------------------------------------------------------------------------
USER_ID=Cre8design

# programs
MONGOD_PKG=mongodb-osx-ssl-x86_64-4.0.4
MONGOD_PKG_URL=https://fastdl.mongodb.org/osx/mongodb-osx-ssl-x86_64-4.0.4.tgz
NODE_PKG=node-v10.14.1
NODE_PKG_URL=https://nodejs.org/dist/v10.14.1/node-v10.14.1.pkg

# required
LOCALE=
API_KEY=
ADDRESS_ID=

# optional
REPO="https://Culth@bitbucket.org/Culth/wandkraft-display.git"
BRANCH="master"
ENV="production"
MONGODB_URI="mongodb://localhost:27017/display"
PORT="80"
API_REQUEST_URI="https://display.wandkraft.com"

# Parameters
# ------------------------------------------------------------------------------------

for i in "$@"
do
case $i in
    -l=*|--locale=*)
		LOCALE="${i#*=}"
    ;;
    -k=*|--apikey=*)
		API_KEY="${i#*=}"
    ;;
    -a=*|--address=*)
		ADDRESS_ID="${i#*=}"
    ;;
	--repo=*)
		REPO="${i#*=}"
    ;;
	--branch=*)
		BRANCH="${i#*=}"
    ;;
	--mongo=*)
		MONGODB_URI="${i#*=}"
    ;;
	--port=*)
		PORT="${i#*=}"
    ;;
	--baseurl=*)
		API_REQUEST_URI="${i#*=}"
    ;;
    *)
		show_help
    ;;
esac
done

# Validator
# ------------------------------------------------------------------------------------

if [[ -z $API_KEY || -z $ADDRESS_ID || -z $LOCALE || -z $REPO || -z $BRANCH || -z $ENV || -z $MONGODB_URI || -z $PORT || -z $API_REQUEST_URI ]]
	then
		show_help
fi


# Setup
# ------------------------------------------------------------------------------------

# Install Node
curl -O ${NODE_PKG_URL}
installer -pkg ${NODE_PKG}.pkg -target /

# Install MongoDB
curl -O ${MONGOD_PKG_URL}
tar -zxvf ${MONGOD_PKG}.tgz ${MONGOD_PKG}
mkdir -p mongodb
cp -R -n ${MONGOD_PKG}/ mongodb
mv mongodb /usr/local
mkdir -p /var/log/mongodb/
mkdir -p /var/lib/mongodb/
mkdir -p /etc/mongodb/conf/
touch /etc/mongodb/conf/mongod.conf
cat > /etc/mongodb/conf/mongod.conv <<EOM
dbpath=/var/lib/mongodb
logpath=/var/log/mongodb/mongodb.log
logappend=true
EOM
export PATH="/usr/local/mongodb/bin:$PATH"

# Add Mongodb launchtrl plist 
touch "/Library/LaunchDaemons/mongo.plist"
cat > '/Library/LaunchDaemons/mongo.plist' <<EOM
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>mongodb</string>

	<key>RunAtLoad</key>
	<true/>

	<key>KeepAlive</key>
	<true/>

	<key>ProgramArguments</key>
	<array>
		<string>/usr/local/mongodb/bin/mongod</string>
        <string>--config</string>
        <string>/etc/mongodb/conf/mongod.conf</string>
	</array>

	<key>WorkingDirectory</key>
    <string>/usr/local/mongodb</string>

	<key>StandardErrorPath</key>
	<string>/var/log/mongodb/error.log</string>
</dict>
</plist>
EOM

# Load Mongodb launchd process
launchctl load -w "/Library/LaunchDaemons/mongo.plist"

# Cloning Repo 
mkdir -p "/Users/${USER_ID}/display"
git clone "https://Culth@bitbucket.org/Culth/wandkraft-display.git" --branch ${BRANCH} --single-branch"/Users/${USER_ID}/display"
cd "/Users/${USER_ID}/display"
npm install

# Generating dotenv file
touch "/Users/${USER_ID}/display"
cat > "/Users/${USER_ID}/display/.env" <<EOM
NODE_ENV=${ENV}
MONGODB_URI=${MONGODB_URI}
LOCALE=${LOCALE}
PORT=${PORT}

WK_API_REQUEST_URI=${API_REQUEST_URI}
WK_API_KEY=${API_KEY}
WK_ADDRESS_ID=${ADDRESS_ID}
EOM

# Generating Startup bash script
touch "/Users/${USER_ID}/bin/start_display.sh"
cat > "/Users/${USER_ID}/bin/start_display.sh" <<EOM
#!/bin/sh

# Go to base directory
cd "/Users/${USER_ID}/display"

# install node modules
npm install

# Build server side code
npm run ts:build

# Build Client Side Code
npm run ng:build:${LOCALE}

# Sync App with Server
npm run sync

# Pull latest version for next build
git pull origin ${BRANCH}

# Serve Application
npm run serve
EOM

# Generating startup launchd plist
touch "/Library/LaunchDaemons/display.plist"
cat > "/Library/LaunchDaemons/display.plist" <<EOM
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>EnviromentVariables</key>
		<dict>
			<key>PATH</key>
			<string>/usr/local/bin:/usr/bin:/bin:/bin/usr?sbin:/sbin</string>
		</dict>

		<key>Label</key>
		<string>display</string>

		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/Users/${USER_ID}/bin/start_display.sh</string>
		</array>

		<key>StandardErrorPath</key>
		<string>/Users/${USER_ID}/display-error.log</string>
	</dict>
</plist>
EOM

# Loading startup launchd plist
launchctl load -w "/Library/LaunchDaemons/display.plist"

# Run Setup
# ------------------------------------------------------------------------------------

echo "Setup complete"